# Apache-Tomcat-8-Application-Server


#### Installation and Configuration
###### Oracle Java 8 Installation and Setup

Download jdk1.8 oracle site
	su - to root
	cd /opt

	tar zxvf /home/jarkko/Downloads/jdk-8u191-linux-x64.tar.gz
	
	mv jdk-8u191/ jdk8
	cd jdk8 

	alternatives --install /usr/bin/java java /opt/jdk8/java 2
	alternatives --config java
	
	There are XX programs which porvide 'java'
	type selection number 
	
	alternatives --install /usr/bin/jar jar /opt/jdk8/jar 2
    alternatives --install /usr/bin/javac javac /opt/jdk8/javac 2	
	
	alternatives --set jar /opt/jdk8/bin/jar
	alternatives --set javac /opt/jdk8/bin/javac
	
	javac -version
	jar -version
	
###### Apache Tomcat 8 Installation
	
Dowload latest apache-tomcat8 version 8.5.34
	
	cd /opt
	tar zxvf /home/jarkko/Downloads/apache-tomcat-8.5.34.tar.gz
	
	mv apache-tomcat-8.5.34/ tomcat8
	cd tomcat8
	
	cd bin 
	
	export CATALINA_HOME=/opt/tomcat8/
	set|grep CATALINA
	
	./startup
	
	ps aux|grep start
	
	localhost:8080 or ip:8080
	Apache tomcat/8.5.34 homepage should appear

###### Apache Tomcat 8 Environment Setup
	
	./shutdown 

	Add couple environment variables /etc/profile. Add couple line end of file
	
	vim /etc/profile

	...
	CATALINA_BASE=/opt/tomcat8
	CATALINA_HOME=/opt/tomcat8
	CATALINA_TMPDIR=/opt/tomcat8/temp
	JAVA_HOME=/opt/jdk8
	JRE_HOME=/opt8/jdk8

	[root@localhost ~]# set|grep CATALINA
	CATALINA_BASE=/opt/tomcat8
	CATALINA_HOME=/opt/tomcat8
	CATALINA_TMPDIR=/opt/tomcat8/temp
	
	[root@localhost ~]# set|grep JRE
	JRE_HOME=/opt/jdk8

	[root@localhost bin]# ./catalina.sh start
	Using CATALINA_BASE:   /opt/tomcat8
	Using CATALINA_HOME:   /opt/tomcat8
	Using CATALINA_TMPDIR: /opt/tomcat8/temp
	Using JRE_HOME:        /
	Using CLASSPATH:       /opt/tomcat8/bin/bootstrap.jar:/opt/tomcat8/bin/tomcat-juli.jar
	Tomcat started.

	tomcat can sptoped command
	./catalina.sh stop  or ./catalina.sh stop -force

######	Customizing the Java Virtual Machine

	in /opt/tomcat8/bin/catalina.sh
	
	change CATALINA_OPTS variable. Set up menory max and min
	CATALINA_OPTS="$JPDA_OPTS $CATALINA_OPTS -Mms128m -Xmx256m"

	[root@localhost bin]# ps aux|grep tomcat
	root      9139  0.9  3.2 3269484 94108 pts/0   Sl   17:33   0:07 //bin/java -Djava.util.logging.config.file=/opt/tomcat8/conf/logging.properties -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager -Djdk.tls.ephemeralDHKeySize=2048 -Djava.protocol.handler.pkgs=org.apache.catalina.webresources -Dorg.apache.catalina.security.SecurityListener.UMASK=0027 -Dignore.endorsed.dirs= -classpath /opt/tomcat8/bin/bootstrap.jar:/opt/tomcat8/bin/tomcat-juli.jar -Dcatalina.base=/opt/tomcat8 -Dcatalina.home=/opt/tomcat8 -Djava.io.tmpdir=/opt/tomcat8/temp org.apache.catalina.startup.Bootstrap start
	root      9394  0.0  0.0 112704   972 pts/0    R+   17:46   0:00 grep --color=auto tomcat		

# Deploying Applications and Managing Tomcat

###### Security and User Accounts

Change tomcat-users.xml uncomment roles

[root@localhost conf]# vim tomcat-users.xml 

edit roles first original
...
<role rolename="tomcat"/>
<role rolename="role1"/>
<user username="tomcat" password="<must-be-changed>" roles="tomcat"/>
<user username="both" password="<must-be-changed>" roles="tomcat,role1"/>
<user username="role1" password="<must-be-changed>" roles="role1"/>

add role and user

  <role rolename="tomcat"/>
  <role rolename="role1"/>
  <role rolename="manager-gui"/>
  <user username="tomcat" password="<must-be-changed>" roles="tomcat"/>
  <user username="both" password="<must-be-changed>" roles="tomcat,role1"/>
  <user username="role1" password="<must-be-changed>" roles="role1"/>
  <user username="manager" password="manager" roles="manager-gui"/>

and restart tomcat 

[root@localhost conf]# ../bin/catalina.sh start

###### Managing applications

You can managing installation  when edit /opt/tomcat8/conf/server.xml file

[root@localhost conf]# vim server.xml

default value is

<Server port="8005" shutdown="SHUTDOWN">

You can shutdown tomcat 8 server typing command promt

[root@localhost conf]# telnet localhost 8005

[root@localhost conf]# ps aux|grep tomcat
root      3815  0.0  0.0 112704   968 pts/0    R+   13:15   0:00 grep --color=auto tomcat

If you have many administrator you can edit server.xml and change shutsown xommand. 

<Server port="8005" shutdown="SHUTDOWN"> command e.g

<Server port="8005" shutdown="SH#TD#WN"> 

###### Connection Proxy

Installing nginx 

yum install epel-release
yum install nginx


[root@localhost ~]# cd /etc/nginx/

[root@localhost nginx]# ll
total 60
drwxr-xr-x. 2 root root    6 Mar  6  2018 conf.d
drwxr-xr-x. 2 root root    6 Mar  6  2018 default.d
-rw-r--r--. 1 root root 1077 Mar  6  2018 fastcgi.conf
-rw-r--r--. 1 root root 1077 Mar  6  2018 fastcgi.conf.default
-rw-r--r--. 1 root root 1007 Mar  6  2018 fastcgi_params
-rw-r--r--. 1 root root 1007 Mar  6  2018 fastcgi_params.default
-rw-r--r--. 1 root root 2837 Mar  6  2018 koi-utf
-rw-r--r--. 1 root root 2223 Mar  6  2018 koi-win
-rw-r--r--. 1 root root 3957 Mar  6  2018 mime.types
-rw-r--r--. 1 root root 3957 Mar  6  2018 mime.types.default
-rw-r--r--. 1 root root 2467 Mar  6  2018 nginx.conf
-rw-r--r--. 1 root root 2656 Mar  6  2018 nginx.conf.default
-rw-r--r--. 1 root root  636 Mar  6  2018 scgi_params
-rw-r--r--. 1 root root  636 Mar  6  2018 scgi_params.default
-rw-r--r--. 1 root root  664 Mar  6  2018 uwsgi_params
-rw-r--r--. 1 root root  664 Mar  6  2018 uwsgi_params.default
-rw-r--r--. 1 root root 3610 Mar  6  2018 win-utf

[root@localhost nginx]# mkdir vhost.d

root@localhost nginx]# vim nginx.conf

and lines in nginx.conf file

 include /etc/nginx/vhost.d/*.conf;

and comment server section 


    server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  _;
        root         /usr/share/nginx/html;

        include /etc/nginx/default.d/*.conf;

        location / {
        }

        error_page 404 /404.html;
            location = /40x.html {
        }

        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
    }

and then change direcotry

[root@localhost nginx]# cd vhost.d/

create file  vim default.conf and type

server {
        listen 80;

        root /opt/tomcat8/webapps/sample/;
        index index.php index.html index.htm;

        server_name localhost;

        location /{
        try_files $uri $uri/ /index.php;
        }

        location ~ \.php$ {

        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header Host $host;
        proxy_pass http://127.0.0.1:8080;
        }
        location ~/\.ht{
                deny all;
        }
}

start nginx

[root@localhost vhost.d]# service nginx start
Redirecting to /bin/systemctl start nginx.service


